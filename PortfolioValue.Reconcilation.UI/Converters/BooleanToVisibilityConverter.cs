﻿using System;
using System.Globalization;
using System.Windows;   
using System.Windows.Data;

namespace PortfolioValue.Reconcilation.UI.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                bool val = (bool)value;
                return val ? Visibility.Visible : Visibility.Collapsed;

            }
            return Visibility.Collapsed;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
