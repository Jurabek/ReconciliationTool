﻿using System;
using System.IO;
using Microsoft.Win32;
using PortfolioValue.Reconcilation.Abstractions.Loaders;
using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.UI.Loaders
{
    public class FileLoader : IFileLoader
    {
        public void Open(Action<string, FileType> openedFile)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "Data files (*.csv, *.xlsx)  | *.csv; *.xlsx"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                var ext = Path.GetExtension(openFileDialog.FileName);
                if (ext == ".csv")
                {
                    openedFile?.Invoke(openFileDialog.FileName, FileType.Csv);
                }
                else if (ext == ".xlsx")
                {
                    openedFile?.Invoke(openFileDialog.FileName, FileType.Excel);
                }
                else if (ext == ".xls")
                {
                    openedFile?.Invoke(openFileDialog.FileName, FileType.Excel);
                }
            }
        }
    }
}
