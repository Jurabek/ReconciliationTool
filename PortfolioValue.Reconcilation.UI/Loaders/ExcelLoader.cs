using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper.Excel;
using PortfolioValue.Reconcilation.Abstractions.Loaders;
using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.UI.Loaders
{
	public class ExcelLoader : IExcelLoader
	{
		public Task<ObservableCollection<dynamic>> GetData(string filePath, FileType fileType)
		{
			return Task.Factory.StartNew(() =>
			{
				ObservableCollection<dynamic> result = null;
				if (fileType == FileType.Excel)
				{
					using (var excelParser = new ExcelParser(filePath))
					{
						ILedgerLoader loader = new LedgerLoader();
						var source = loader.Load(excelParser).ToList();
						result = new ObservableCollection<dynamic>(source);
					}
				}
				else if (fileType == FileType.Csv)
				{
					using (StreamReader reader = new StreamReader(filePath))
					{
						ILedgerLoader ledgerLoader = new LedgerLoader();
						var source = ledgerLoader.Load(reader);
						result = new ObservableCollection<dynamic>(source);
					}
				}
				return result;
			});
		}
	}
}