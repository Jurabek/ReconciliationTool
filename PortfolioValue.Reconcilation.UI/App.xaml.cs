﻿using System.Windows;
using Microsoft.Practices.Unity;
using PortfolioValue.Reconcilation.ViewModels;

namespace PortfolioValue.Reconcilation.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var bootstrapper = new Bootsrapper();
            var container = bootstrapper.Build();

            var mainViewModel = container.Resolve<MainViewModel>();

            MainWindow = new MainWindow(mainViewModel);
            MainWindow.Show();
            base.OnStartup(e);
        }
    }
}
