﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace PortfolioValue.Reconcilation.UI.Helpers
{
    public class DataGridSelectedItemsBehavior : Behavior<DataGrid>
    {
        protected override void OnAttached()
        {
            AssociatedObject.SelectionChanged += AssociatedObjectSelectionChanged;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.SelectionChanged -= AssociatedObjectSelectionChanged;
        }

        void AssociatedObjectSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
			//var array = new object[AssociatedObject.SelectedItems.Count];
			//AssociatedObject.SelectedItems.CopyTo(array, 0);
			//SelectedItems = array;
		}

        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(IEnumerable), typeof(DataGridSelectedItemsBehavior),
            new FrameworkPropertyMetadata(null, PropertyChanged));


        private static void PropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            IEnumerable selectedItems = e.NewValue as IEnumerable;
            if (selectedItems != null)
            {
                foreach (var selectedItem in selectedItems)
                {
                    DataGridSelectedItemsBehavior behavior = source as DataGridSelectedItemsBehavior;
                    if (behavior != null)
                    {
                        DataGrid dataGrid = behavior.AssociatedObject;
                        dataGrid.SelectedItems.Add(selectedItem);
                    }
                }
            }
        }

        public IEnumerable SelectedItems
        {
            get { return (IEnumerable)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }
    }
}
