﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace PortfolioValue.Reconcilation.UI.Helpers
{
    public static class DataGridHelper
    {
        public static bool GetIsDateColumn(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsDateColumnProperty);
        }

        public static void SetIsDateColumn(DependencyObject obj, bool value)
        {
            obj.SetValue(IsDateColumnProperty, value);
        }

        public static readonly DependencyProperty IsDateColumnProperty =
            DependencyProperty.RegisterAttached("IsDateColumn", typeof(bool), typeof(DataGridHelper), new PropertyMetadata(false, IsDateColumnPropertyChangedCallback));

        private static void IsDateColumnPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if ((bool)dependencyPropertyChangedEventArgs.NewValue)
            {
				
            }
        }

        public static bool GetIsKeyColumn(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsKeyColumnProperty);
        }

        public static void SetIsKeyColumn(DependencyObject obj, bool value)
        {
            obj.SetValue(IsKeyColumnProperty, value);
        }

        public static readonly DependencyProperty IsKeyColumnProperty =
            DependencyProperty.RegisterAttached("IsKeyColumn", typeof(bool), typeof(DataGridHelper), new PropertyMetadata(false));
		
        public static bool GetIsAmountColumn(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsAmountColumnProperty);
        }

        public static void SetIsAmountColumn(DependencyObject obj, bool value)
        {
            obj.SetValue(IsAmountColumnProperty, value);
        }

        public static readonly DependencyProperty IsAmountColumnProperty =
            DependencyProperty.RegisterAttached("IsAmountColumn", typeof(bool), typeof(DataGridHelper), new PropertyMetadata(false));


        public static ObservableCollection<string> GetProperties(DependencyObject obj)
        {
            return (ObservableCollection<string>)obj.GetValue(PropertiesProperty);
        }

        public static void SetProperties(DependencyObject obj, ObservableCollection<string> value)
        {
            obj.SetValue(PropertiesProperty, value);
        }

        public static readonly DependencyProperty PropertiesProperty =
            DependencyProperty.RegisterAttached("Properties",
                typeof(ObservableCollection<string>), typeof(DataGridHelper),
                new UIPropertyMetadata(null, BindableColumnsPropertyChanged));


        private static void BindableColumnsPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            DataGrid dataGrid = source as DataGrid;
            ObservableCollection<string> properties = e.NewValue as ObservableCollection<string>;
            if (dataGrid != null)
            {
                if (properties == null)
                    return;

                dataGrid.Columns.Clear();
                foreach (string property in properties.Where(p => p != "IsMatch"))
                {
                    dataGrid.Columns.Add(CreateColumnFromProperty(property));
                }

                properties.CollectionChanged += (sender, e2) =>
                {
                    NotifyCollectionChangedEventArgs ne = e2;
                    if (ne.Action == NotifyCollectionChangedAction.Reset)
                    {
                        dataGrid.Columns.Clear();
                        foreach (string property in ne.NewItems)
                        {
                            if (property == "IsMatch")
                                continue;

                            dataGrid.Columns.Add(CreateColumnFromProperty(property));
                        }
                    }
                    else if (ne.Action == NotifyCollectionChangedAction.Add)
                    {
                        foreach (string property in ne.NewItems)
                        {
                            if (property == "IsMatch")
                                continue;

                            dataGrid.Columns.Add(CreateColumnFromProperty(property));
                        }
                    }
                    else if (ne.Action == NotifyCollectionChangedAction.Move)
                    {
                        dataGrid.Columns.Move(ne.OldStartingIndex, ne.NewStartingIndex);
                    }
                    else if (ne.Action == NotifyCollectionChangedAction.Remove)
                    {
                        foreach (string property in ne.OldItems)
                        {
                            var column = dataGrid.Columns.FirstOrDefault(c => (string)c.Header == property);
                            dataGrid.Columns.Remove(column);
                        }
                    }
                    else if (ne.Action == NotifyCollectionChangedAction.Replace)
                    {
                        dataGrid.Columns[ne.NewStartingIndex] = ne.NewItems[0] as DataGridColumn;
                    }
                };
            }
        }
		
        private static DataGridColumn CreateColumnFromProperty(string property)
        {   // now set up a column and binding for each property
            var column = new DataGridTextColumn
            {
                Header = property,
                Binding = new Binding(property)
            };
            return column;
        }
		
    }
}
