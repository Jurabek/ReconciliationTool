﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace PortfolioValue.Reconcilation.UI.Helpers
{
	public static class DataGridVisualTreeHelper
	{
		private static DataGridColumnHeader TempColumnHeader;

		public static DataGridColumnHeader GetColumnHeader(this DataGridColumn column, DataGrid dataGrid)
		{
			List<DataGridColumnHeader> columnHeaders = GetVisualChildCollection<DataGridColumnHeader>(dataGrid);
			foreach (DataGridColumnHeader columnHeader in columnHeaders)
			{
				if (Equals(columnHeader.Column, column))
				{
					return columnHeader;
				}
			}
			return null;
		}

		public static DataGridColumnHeader GetDataGridColumnHeader(object sender)
		{
			DataGridColumnHeader columnHeader;

			if (!(((ContextMenu)((MenuItem)sender)?.Parent)?.PlacementTarget is DataGrid dataGrid))
				return null;

			if (TempColumnHeader != null)
			{
				columnHeader = TempColumnHeader;
			}
			else
			{
				var column = dataGrid.CurrentColumn;
				columnHeader = GetColumnHeader(column, dataGrid);
			}
			columnHeader.Tag = dataGrid.DataContext;
			return columnHeader;
		}

		public static void FindDataGridHeaderByMouseClick(RoutedEventArgs e)
		{
			TempColumnHeader = null;
			DependencyObject depObject = (DependencyObject)e.OriginalSource;

			while ((depObject != null) && !(depObject is DataGridColumnHeader))
			{
				depObject = VisualTreeHelper.GetParent(depObject);
			}

			if (depObject == null)
				return;

			TempColumnHeader = depObject as DataGridColumnHeader;
		}

		private static List<T> GetVisualChildCollection<T>(object parent) where T : Visual
		{
			List<T> visualCollection = new List<T>();
			GetVisualChildCollection(parent as DependencyObject, visualCollection);
			return visualCollection;
		}

		private static void GetVisualChildCollection<T>(DependencyObject parent, List<T> visualCollection) where T : Visual
		{
			int count = VisualTreeHelper.GetChildrenCount(parent);
			for (int i = 0; i < count; i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(parent, i);
				if (child is T)
				{
					visualCollection.Add(child as T);
				}
				else
				{
					GetVisualChildCollection(child, visualCollection);
				}
			}
		}
	}
}
