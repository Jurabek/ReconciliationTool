﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dynamitey;

namespace PortfolioValue.Reconcilation.UI.Helpers
{
    public static class DynamicTypeHelper
    {
        public static Dictionary<string, Type> GetDataTypes(this IEnumerable<dynamic> data)
        {
            dynamic source = data.FirstOrDefault();
            if (source == null)
                return null;

            Dictionary<string, Type> result = new Dictionary<string, Type>();
            IEnumerable<string> properties = Dynamic.GetMemberNames(source);

            foreach (string property in properties)
            {
                if (property == "IsMatch")
                    continue;

                var sourceData = data.FirstOrDefault(s => !string.IsNullOrEmpty(Dynamic.InvokeGet(s, property)));
                if (sourceData == null)
                    continue;

                string propertyValue = Dynamic.InvokeGet(sourceData, property);

                DateTime dateTime;


                if (DateTime.TryParse(propertyValue, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                {
                    result.Add(property, typeof(DateTime));
                    continue;
                }

                var ci = CultureInfo.GetCultures(CultureTypes.AllCultures);
                var allpatterns = ci.SelectMany(c => c.DateTimeFormat.GetAllDateTimePatterns());
                string[] formats = allpatterns.ToArray();
                

                if (DateTime.TryParseExact(propertyValue, formats, CultureInfo.InvariantCulture, DateTimeStyles.None,
                    out dateTime))
                {
                    result.Add(property, typeof(DateTime));
                    continue;
                }
                
                decimal decimalValue;
                if (decimal.TryParse(propertyValue, out decimalValue))
                {
                    result.Add(property, typeof(decimal));
                    continue;
                }

                long longValue;
                if (long.TryParse(propertyValue, out longValue))
                {
                    result.Add(property, typeof(long));
                    continue;
                }

                double doubleValue;
                if (double.TryParse(propertyValue, out doubleValue))
                {
                    result.Add(property, typeof(double));
                    continue;
                }

                bool boolValue;
                if (bool.TryParse(propertyValue, out boolValue))
                {
                    result.Add(property, typeof(bool));
                    continue;
                }

                TimeSpan timeSpanValue;
                if (TimeSpan.TryParse(propertyValue, out timeSpanValue))
                {
                    result.Add(property, typeof(TimeSpan));
                }

                result.Add(property, typeof(string));
            }
            return result;
        }
    }
}
