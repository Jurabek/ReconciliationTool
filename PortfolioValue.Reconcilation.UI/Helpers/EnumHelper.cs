﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Markup;

namespace PortfolioValue.Reconcilation.UI.Helpers
{
    public static class EnumHelper
    {
        public static string Description(this Enum eValue)
        {
            var nAttributes = eValue.GetType().GetField(eValue.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (nAttributes.Any())
            {
                DescriptionAttribute descriptionAttribute = nAttributes.First() as DescriptionAttribute;
                if (descriptionAttribute != null)
                    return descriptionAttribute.Description;
            }
            return String.Empty;
        }
        
    }

    public class ValueDescription
    {
        public Enum Value { get; set; }
        public string Description { get; set; }
    }

    public class EnumToItemsSource : MarkupExtension
    {
        private readonly Type _type;

        public EnumToItemsSource(Type type)
        {
            _type = type;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Enum.GetValues(_type)
                .Cast<Enum>()
                .Select(e => new {Value = e, Description = e.Description()});
        }
    }
}
