﻿using Microsoft.Practices.Unity;
using PortfolioValue.Reconcilation.Abstractions.Adapters;
using PortfolioValue.Reconcilation.Abstractions.Loaders;
using PortfolioValue.Reconcilation.Abstractions.Providers;
using PortfolioValue.Reconcilation.Abstractions.ViewModels;
using PortfolioValue.Reconcilation.Adapters;
using PortfolioValue.Reconcilation.Providers;
using PortfolioValue.Reconcilation.Repositories;
using PortfolioValue.Reconcilation.UI.Loaders;
using PortfolioValue.Reconcilation.ViewModels;

namespace PortfolioValue.Reconcilation.UI
{
    public sealed class Bootsrapper
    {
        public static IUnityContainer Container { get; private set; }

        public IUnityContainer Build()
        {
            var container = new UnityContainer();
            container.RegisterType<IFileLoader, FileLoader>(new ContainerControlledLifetimeManager());
            container.RegisterType<IExcelLoader, ExcelLoader>(new ContainerControlledLifetimeManager());
            container.RegisterType<DestinationItemsViewModel, DestinationItemsViewModel>(new ContainerControlledLifetimeManager());
            container.RegisterType<SourceItemsViewModel, SourceItemsViewModel>(new ContainerControlledLifetimeManager());
            container.RegisterType<IReconcilationViewModel, ReconcilationViewModel>(new ContainerControlledLifetimeManager());
            container.RegisterType<MainViewModel, MainViewModel>(new ContainerControlledLifetimeManager());
            container.RegisterType<ToleranceViewModel, ToleranceViewModel>(new ContainerControlledLifetimeManager());
            container.RegisterType<ResultViewModel, ResultViewModel>(new ContainerControlledLifetimeManager());
            container.RegisterType<ISettingsRepository, SettingsRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<ISelectedPropertiesAdapter, SelectedPropertiesAdapter>(new ContainerControlledLifetimeManager());
	        container.RegisterType<IReconciliationProvider, ReconciliationProvider>(new ContainerControlledLifetimeManager());
	        container.RegisterType<IDatePropertyProvider, DatePropertyProvider>(new ContainerControlledLifetimeManager());
	        container.RegisterType<IAmountPropertyProvider, AmountPropertyProvider>(new ContainerControlledLifetimeManager());
	        container.RegisterType<IKeyComparerProvider, KeyComparerProvider>(new ContainerControlledLifetimeManager());

			Container = container;
            return container;
        }
    }
}
