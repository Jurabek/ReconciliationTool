﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using PortfolioValue.Reconcilation.UI.Helpers;
using PortfolioValue.Reconcilation.ViewModels;
using ReactiveUI;
using System;
using System.Reactive.Linq;

namespace PortfolioValue.Reconcilation.UI
{
	// ReSharper disable once RedundantExtendsListEntry
	public partial class MainWindow : Window
	{
		public MainViewModel ViewModel { get; }

		public MainWindow(MainViewModel mainViewModel)
		{
			InitializeComponent();
			ViewModel = mainViewModel;
			DataContext = ViewModel;

			MessageBus.Current.Listen<Exception>()
				.Subscribe(ex =>
				{
					MessageBox.Show(ex.Message);
				});


			this.WhenAnyObservable(x => x.ViewModel.SourceItemsViewModel.DatePropertyChanged)
				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(dateProperty =>
				{
					var column = SourceDataGrid.Columns.SingleOrDefault(c => c.Header.ToString() == dateProperty);
					var columnHeader = column.GetColumnHeader(SourceDataGrid);
					DataGridHelper.SetIsDateColumn(columnHeader, true);
				});

			this.WhenAnyObservable(x => x.ViewModel.DestinationItemsViewModel.DatePropertyChanged)
				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(dateProperty =>
				{
					var column = DestinationDataGrid.Columns.SingleOrDefault(c => c.Header.ToString() == dateProperty);
					var columnHeader = column.GetColumnHeader(DestinationDataGrid);
					DataGridHelper.SetIsDateColumn(columnHeader, true);
				});

			this.WhenAnyObservable(x => x.ViewModel.SourceItemsViewModel.KeyPropertyChanged)
				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(keyProperty =>
				{
					var column = SourceDataGrid.Columns.SingleOrDefault(c => c.Header.ToString() == keyProperty);
					var columnHeader = column.GetColumnHeader(SourceDataGrid);
					DataGridHelper.SetIsKeyColumn(columnHeader, true);
				});

			this.WhenAnyObservable(x => x.ViewModel.DestinationItemsViewModel.KeyPropertyChanged)
				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(keyProperty =>
				{
					var column = DestinationDataGrid.Columns.SingleOrDefault(c => c.Header.ToString() == keyProperty);
					var columnHeader = column.GetColumnHeader(DestinationDataGrid);
					DataGridHelper.SetIsKeyColumn(columnHeader, true);
				});

			this.WhenAnyObservable(x => x.ViewModel.DestinationItemsViewModel.AmountProprtiesChanged)
				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(amountProperties =>
				{
					foreach (var amountProperty in amountProperties)
					{
						var column = DestinationDataGrid.Columns.SingleOrDefault(c => c.Header.ToString() == amountProperty);
						var columnHeader = column.GetColumnHeader(DestinationDataGrid);
						DataGridHelper.SetIsAmountColumn(columnHeader, true);
					}
				});

			this.WhenAnyObservable(x => x.ViewModel.SourceItemsViewModel.AmountProprtiesChanged)
				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(amountProperties =>
				{
					foreach (var amountProperty in amountProperties)
					{
						var column = SourceDataGrid.Columns.SingleOrDefault(c => c.Header.ToString() == amountProperty);
						var columnHeader = column.GetColumnHeader(SourceDataGrid);
						DataGridHelper.SetIsAmountColumn(columnHeader, true);
					}
				});
		}

		private void MenuDate_Clicked(object sender, RoutedEventArgs e)
		{
			ResetColumns(sender, DataGridHelper.SetIsDateColumn);

			var columnHeader = DataGridVisualTreeHelper.GetDataGridColumnHeader(sender);
			var propertyName = columnHeader.DataContext as string;

			if (!(columnHeader.Tag is BaseItemsViewModel viewModel))
				return;

			if (DataGridHelper.GetIsAmountColumn(columnHeader))
			{
				DataGridHelper.SetIsAmountColumn(columnHeader, false);
				viewModel.RemoveAmountProperty(propertyName);
			}
			else if (DataGridHelper.GetIsKeyColumn(columnHeader))
			{
				DataGridHelper.SetIsKeyColumn(columnHeader, false);
				viewModel.KeyProperty = null;
			}

			viewModel.DateProperty = propertyName;
			DataGridHelper.SetIsDateColumn(columnHeader, true);
		}

		private void MenuKey_Clicked(object sender, RoutedEventArgs e)
		{
			ResetColumns(sender, DataGridHelper.SetIsKeyColumn);

			var columnHeader = DataGridVisualTreeHelper.GetDataGridColumnHeader(sender);
			var propertyName = columnHeader.DataContext as string;

			if (!(columnHeader.Tag is BaseItemsViewModel viewModel))
				return;

			if (DataGridHelper.GetIsDateColumn(columnHeader))
			{
				DataGridHelper.SetIsDateColumn(columnHeader, false);
				viewModel.DateProperty = null;
			}

			if (DataGridHelper.GetIsAmountColumn(columnHeader))
			{
				DataGridHelper.SetIsAmountColumn(columnHeader, false);
				viewModel.RemoveAmountProperty(propertyName);
			}

			viewModel.KeyProperty = propertyName;
			DataGridHelper.SetIsKeyColumn(columnHeader, true);
		}

		private void MenuAmount_Clicked(object sender, RoutedEventArgs e)
		{
			var columnHeader = DataGridVisualTreeHelper.GetDataGridColumnHeader(sender);
			var propertyName = columnHeader.DataContext as string;

			if (!(columnHeader.Tag is BaseItemsViewModel viewModel))
				return;

			if (DataGridHelper.GetIsDateColumn(columnHeader))
			{
				DataGridHelper.SetIsDateColumn(columnHeader, false);
				viewModel.DateProperty = null;
			}
			else if (DataGridHelper.GetIsKeyColumn(columnHeader))
			{
				DataGridHelper.SetIsKeyColumn(columnHeader, false);
				viewModel.KeyProperty = null;
			}

			viewModel.AddAmountProperty(propertyName);
			DataGridHelper.SetIsAmountColumn(columnHeader, true);
		}

		private void MenuClear_Clicked(object sender, RoutedEventArgs e)
		{
			var columnHeader = DataGridVisualTreeHelper.GetDataGridColumnHeader(sender);
			if (!(columnHeader.Tag is BaseItemsViewModel viewModel))
				return;

			if (DataGridHelper.GetIsDateColumn(columnHeader))
			{
				DataGridHelper.SetIsDateColumn(columnHeader, false);
				viewModel.DateProperty = null;
			}
			else if (DataGridHelper.GetIsAmountColumn(columnHeader))
			{
				DataGridHelper.SetIsAmountColumn(columnHeader, false);
				viewModel.RemoveAmountProperty(columnHeader.Content as string);
			}
			else if (DataGridHelper.GetIsKeyColumn(columnHeader))
			{
				DataGridHelper.SetIsKeyColumn(columnHeader, false);
				viewModel.KeyProperty = null;
			}
		}

		private void SourceDataGrid_OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			DataGridVisualTreeHelper.FindDataGridHeaderByMouseClick(e);
		}

		private void DestinationDataGrid_OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			DataGridVisualTreeHelper.FindDataGridHeaderByMouseClick(e);
		}

		private void ResetColumns(object sender, Action<DependencyObject, bool> setColumnAction)
		{
			if (!(((ContextMenu) ((MenuItem) sender)?.Parent)?.PlacementTarget is DataGrid dataGrid))
				return;

			foreach (var column in dataGrid.Columns)
			{
				var header = column.GetColumnHeader(dataGrid);
				setColumnAction.Invoke(header, false);
			}
		}
	}
}
