﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiColumnCombobox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Accounts = new ObservableCollection<Account>
            {
                new Account { AccountNumber  ="ABC", ClientName = "AAA" },
                new Account { AccountNumber  ="ABD", ClientName = "AAB" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
                new Account { AccountNumber  ="ABA", ClientName = "AAN" },
            };
            
            this.DataContext = this;
            
        }

        public ObservableCollection<Account> Accounts { get; set; }

        private void ComboBox_Selected(object sender, RoutedEventArgs e)
        {
            test.IsDropDownOpen = false;
        }
    }

    public class Account
    {
        public string AccountNumber { get; set; }
        public string CurrencyCode { get; set; }
        public string ClientName { get; set; }
    }
}
