﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MultiColumnCombobox
{
    public static class ComboBoxDataGridHelper
    {
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        public static DataGrid GetComboBoxDataGrid(DependencyObject obj)
        {
            return (DataGrid)obj.GetValue(ComboBoxDataGridProperty);
        }

        public static void SetComboBoxDataGrid(DependencyObject obj, DataGrid value)
        {
            obj.SetValue(ComboBoxDataGridProperty, value);
        }

        public static readonly DependencyProperty ComboBoxDataGridProperty =
            DependencyProperty.RegisterAttached("ComboBoxDataGrid", typeof(DataGrid), typeof(ComboBoxDataGridHelper), new PropertyMetadata(null));
    }
}
