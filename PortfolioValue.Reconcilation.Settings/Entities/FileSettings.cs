﻿using System.Collections.Generic;

namespace PortfolioValue.Reconcilation.Settings.Entities
{
    public sealed class FileSettings
    {
        public FileSettings()
        {
            AmountProperties = new List<AmountProperty>();
        }
        public int Id { get; set; }

        public string FilePath { get; set; }

        public string DateProperty { get; set; }

        public string KeyProperty { get; set; }

        public List<AmountProperty> AmountProperties { get; set; }

    }
}
