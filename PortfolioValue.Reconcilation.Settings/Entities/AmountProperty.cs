﻿namespace PortfolioValue.Reconcilation.Settings.Entities
{
    public class AmountProperty
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int FileSettingsId { get; set; }

        public virtual FileSettings FileSettings { get; set; }
    }
}
