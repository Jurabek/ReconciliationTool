﻿using Microsoft.EntityFrameworkCore;
using PortfolioValue.Reconcilation.Settings.Entities;

namespace PortfolioValue.Reconcilation.Settings
{
    public sealed class SettingsDbContext : DbContext
    {
        public SettingsDbContext()
        {
	        Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename = settings.db");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AmountProperty>()
                .HasOne(p => p.FileSettings)
                .WithMany(s => s.AmountProperties)
                .HasForeignKey(s => s.FileSettingsId);

            modelBuilder.Entity<FileSettings>()
                .HasAlternateKey(c => c.FilePath)
                .HasName("AlternateKey_FilePath");

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<FileSettings> Settings { get; set; }
    }
}
