﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace PortfolioValue.Reconcilation
{
    public static class DynamicHelper
    {
        internal static dynamic Copy(dynamic original)
        {
            var clone = new ExpandoObject();

            var originalDictionary = (IDictionary<string, object>)original;
            var cloneDictionary = (IDictionary<string, object>)clone;

            foreach (var kvp in originalDictionary)
                cloneDictionary.Add(kvp.Key, kvp.Value is ExpandoObject ? Copy((ExpandoObject)kvp.Value) : kvp.Value);

            return clone;
        }

	    internal static IEnumerable<dynamic> Copy(this IEnumerable<dynamic> original)
	    {
		    return original.Select(o => Copy(o));
	    }
    }
}
