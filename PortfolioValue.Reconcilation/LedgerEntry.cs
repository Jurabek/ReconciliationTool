﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PortfolioValue.Reconcilation
{
    /// <summary>
    /// Represents movement in a single currency
    /// </summary>
    public class LedgerEntry : ReactiveObject
    {
        private bool _isMatched;
        public bool IsMatched
        {
            get { return _isMatched; }
            set { this.RaiseAndSetIfChanged(ref _isMatched, value); }
        }

        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }



        public bool HasMatchIn(List<LedgerEntry> ledger, ReconciliatorOptions options)
        {
            // probably should have a strategy pattern here for various matching methods ?
            switch (options.MatchingMethod)
            {
                case MatchingMethodEnum.OneToOne:
                    {
                        return ledger.Any(e => IsMatch(e, this, options));
                    }

                //case MatchingMethodType.OneToMany:
                //    {
                //        // aggregate flows that are adjacent for the same description (usually the case for variation marging 
                //        var groupings = ledger.GroupAdjacent(e => e.Description, (key, group) => group.Sum(v => v.AmountProperty), StringComparer.OrdinalIgnoreCase);
                //        return ledger.Any(e => IsMatch(e, this, options));

                //    }


                default:
                    throw new Exception($"matching method: {options.MatchingMethod} is not supported");
            }

        }
        
        private bool IsMatch(LedgerEntry entry1, LedgerEntry entry2, ReconciliatorOptions options)
        {


            if (entry1.Date == null || entry2.Date == null)
                return false;

            if (entry1.Amount == null || entry1.Amount == null)
                return false;

            // read tolerance fileSettings here
            double? dayDiff = Math.Abs((entry1.Date - entry2.Date).Value.TotalDays);
            decimal? amountDiff = Math.Abs(entry1.Amount.Value - entry2.Amount.Value);

            // compare with tolerance
            var isMatch = dayDiff <= options.DaysTolerance
                && amountDiff <= options.AmountTolerance;

            entry1.IsMatched = isMatch;
            entry2.IsMatched = isMatch;

            return isMatch;

        }
    }

}
