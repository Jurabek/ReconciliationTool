﻿using PortfolioValue.Reconcilation.Abstractions.Adapters;
using PortfolioValue.Reconcilation.Models;
using PortfolioValue.Reconcilation.ViewModels;

namespace PortfolioValue.Reconcilation.Adapters
{
	public class SelectedPropertiesAdapter : ISelectedPropertiesAdapter
	{
		public SelectedProperties GetSelectedProperties(
			SourceItemsViewModel sourceItemsViewModel, 
			DestinationItemsViewModel destinationItemsViewModel, 
			ToleranceViewModel toleranceViewModel, 
			ReconcilationViewModel reconcilationViewModel)
		{
			var result = new SelectedProperties();
			result.DestinationSelectedProperty.KeyProperty = destinationItemsViewModel.KeyProperty;
			result.DestinationSelectedProperty.AmountProperties = destinationItemsViewModel.AmountProperties;
			result.DestinationSelectedProperty.DateProperty = destinationItemsViewModel.DateProperty;

			result.SourceSelectdProperty.DateProperty = sourceItemsViewModel.DateProperty;
			result.SourceSelectdProperty.KeyProperty = sourceItemsViewModel.KeyProperty;
			result.SourceSelectdProperty.AmountProperties = sourceItemsViewModel.AmountProperties;
			result.AmountTolerance = toleranceViewModel.AmountTolerance;
			result.DaysTolerance = toleranceViewModel.DaysTolerance;
			result.SelectedComparerType = reconcilationViewModel.KeyComapererType;
			result.SelectedMatchingMethodType = reconcilationViewModel.MatchingMethodType;

			return result;
		}
	}
}
