﻿using System;
using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.Abstractions.Loaders
{
    public interface IFileLoader
    {
        void Open(Action<string, FileType> openedFile);
    }
}
