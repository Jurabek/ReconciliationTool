﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.Abstractions.Loaders
{
    public interface IExcelLoader
    {
        Task<ObservableCollection<dynamic>> GetData(string filePath, FileType fileType);
    }
}
