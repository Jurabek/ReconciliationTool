﻿namespace PortfolioValue.Reconcilation.Abstractions.Providers
{
	public interface IAmountPropertyProvider
	{
		decimal Parse(dynamic value);

		bool Validate(dynamic item, string propertyName);
	}
}