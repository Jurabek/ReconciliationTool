﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PortfolioValue.Reconcilation.Models;
using PortfolioValue.Reconcilation.ViewModels;

namespace PortfolioValue.Reconcilation.Abstractions.Providers
{
	public interface IReconciliationProvider
	{
		Task<IDictionary<dynamic, dynamic>> Reconcilate(IEnumerable<dynamic> source, IEnumerable<dynamic> destination, SelectedProperties selectedProperties, ToleranceViewModel toleranceViewModel, CancellationToken cancellationToken);
	}
}