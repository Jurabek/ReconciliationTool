﻿using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.Abstractions.Providers
{
	public interface IKeyComparerProvider
	{
		bool Compare(string sourceValue, string destinationValue, KeyComparerType compareType);
	}
}