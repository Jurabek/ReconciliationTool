﻿using System;

namespace PortfolioValue.Reconcilation.Abstractions.Providers
{
	public interface IDatePropertyProvider
	{
		DateTime Parse(dynamic dateTime);
	}
}