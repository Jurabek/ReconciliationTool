﻿using System.Collections.ObjectModel;
using ReactiveUI;

namespace PortfolioValue.Reconcilation.Abstractions.ViewModels
{
    public interface IItemsViewModel
    {
        ReactiveList<string> AmountProperties { get; }
        ObservableCollection<dynamic> Items { get; }
        dynamic SelectedItem { get; }
        string DateProperty { get; }
        bool IsLoading { get; set; }
    }
}