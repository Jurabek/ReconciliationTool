﻿using System.Reactive;
using PortfolioValue.Reconcilation.ViewModels;
using ReactiveUI;

namespace PortfolioValue.Reconcilation.Abstractions.ViewModels
{
    public interface IReconcilationViewModel
    {
        Enums.MatchingMethodType MatchingMethodType { get; set; }
        ReactiveCommand<Unit, Unit> StartReconciliation { get; set; }
        ToleranceViewModel ToleranceViewModel { get; set; }
    }
}