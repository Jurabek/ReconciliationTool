﻿using PortfolioValue.Reconcilation.Models;
using PortfolioValue.Reconcilation.ViewModels;

namespace PortfolioValue.Reconcilation.Abstractions.Adapters
{
	public interface ISelectedPropertiesAdapter
	{
		SelectedProperties GetSelectedProperties(
			SourceItemsViewModel sourceItemsViewModel, 
			DestinationItemsViewModel destinationItemsViewModel, 
			ToleranceViewModel toleranceViewModel, 
			ReconcilationViewModel reconcilationViewModel);
	}
}