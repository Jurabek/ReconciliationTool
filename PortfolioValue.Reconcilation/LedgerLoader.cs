﻿using System.Collections.Generic;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;

namespace PortfolioValue.Reconcilation
{
    
    public class LedgerLoader : ILedgerLoader
    {
        public IEnumerable<dynamic> Load(ICsvParser parser)
        {
            var csv = new CsvReader(parser);
            csv.Configuration.SkipEmptyRecords = true;
            return csv.GetRecords<dynamic>();
        }
        public IEnumerable<object> Load(StreamReader streamReader)
        {
            var csv = new CsvReader(streamReader, new CsvConfiguration() { SkipEmptyRecords = true });
            return csv.GetRecords<dynamic>();
        }
    }
}
