﻿using CsvHelper;
using System.Collections.Generic;
using System.IO;

namespace PortfolioValue.Reconcilation
{
    public interface ILedgerLoader
    {
        IEnumerable<dynamic> Load(StreamReader data);

        IEnumerable<dynamic> Load(ICsvParser parser);
    }
}
