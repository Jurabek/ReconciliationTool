﻿using PortfolioValue.Reconcilation.Abstractions.Providers;
using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.Providers
{
	public class KeyComparerProvider : IKeyComparerProvider
	{
		public bool Compare(string sourceValue, string destinationValue, KeyComparerType compareType)
		{
			switch (compareType)
			{
				case KeyComparerType.ExactMatch:
					return sourceValue.Contains(destinationValue);
				case KeyComparerType.LeftContainsRight:
					return false;
			}
			return false;
		}
	}
}
