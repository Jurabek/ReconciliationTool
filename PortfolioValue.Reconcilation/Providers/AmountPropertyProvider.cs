﻿using System;
using Dynamitey;
using PortfolioValue.Reconcilation.Abstractions.Providers;

namespace PortfolioValue.Reconcilation.Providers
{
	public class AmountPropertyProvider : IAmountPropertyProvider
	{
		public decimal Parse(dynamic value)
		{
			decimal result = 0;
			try
			{
				result = Convert.ToDecimal(value.ToString());
			}
			catch (Exception)
			{// ignored
			}

			return result;
		}

		public bool Validate(dynamic item, string propertyName)
		{
			var itemValue = Dynamic.InvokeGet(item, propertyName);
			if (decimal.TryParse(itemValue.ToString(), out decimal validValue))
			{
				validValue = Math.Abs(validValue);
				Dynamic.InvokeSet(item, propertyName, validValue);
			}
			return false;
		}
	}
}
