﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dynamitey;
using PortfolioValue.Reconcilation.Abstractions.Providers;
using PortfolioValue.Reconcilation.Models;
using PortfolioValue.Reconcilation.ViewModels;

namespace PortfolioValue.Reconcilation.Providers
{
	public class ReconciliationProvider : IReconciliationProvider
	{
		private readonly IDatePropertyProvider _datePropertyProvider;
		private readonly IAmountPropertyProvider _amountPropertyProvider;
		private readonly IKeyComparerProvider _comparerProvider;
		private ToleranceViewModel _toleranceViewModel;
		private IDictionary<dynamic, dynamic> _matchedItems;

		public ReconciliationProvider(
			IDatePropertyProvider datePropertyProvider,
			IAmountPropertyProvider amountPropertyProvider,
			IKeyComparerProvider comparerProvider)
		{
			_datePropertyProvider = datePropertyProvider;
			_amountPropertyProvider = amountPropertyProvider;
			_comparerProvider = comparerProvider;
		}

		public Task<IDictionary<dynamic, dynamic>> Reconcilate(
			IEnumerable<dynamic> source,
			IEnumerable<dynamic> destination,
			SelectedProperties selectedProperties,
			ToleranceViewModel toleranceViewModel,
			CancellationToken cancellationToken)
		{
			_toleranceViewModel = toleranceViewModel;

			return Task.Factory.StartNew(() =>
			{
				_matchedItems = new Dictionary<dynamic, dynamic>();
				foreach (var item in source)
				{
					if (cancellationToken.IsCancellationRequested)
						break;

					var notMatchedItems = destination.Where(x => x.IsMatch == null || x.IsMatch == false);
					var isMatch = HasMatchIn(item, notMatchedItems, selectedProperties);
					item.IsMatch = isMatch;
				}
				return _matchedItems;
			}, cancellationToken);
		}

		private bool HasMatchIn(dynamic entity, IEnumerable<dynamic> external, SelectedProperties selectedProperties)
		{
			return external.Any(e => IsMatch(entity, e, selectedProperties));
		}

		private bool IsMatch(dynamic sourceEntity, dynamic destinationEntity, SelectedProperties selectedProperties)
		{
			var sourceDateValue = Dynamic.InvokeGet(sourceEntity, selectedProperties.SourceSelectdProperty.DateProperty);
			var destinationDateValue = Dynamic.InvokeGet(destinationEntity, selectedProperties.DestinationSelectedProperty.DateProperty);

			var hasKeyValue = false;
			var sourceKeyValue = string.Empty;
			var destKeyValue = string.Empty;

			if (HasKeyValue(selectedProperties))
			{
				sourceKeyValue = Dynamic.InvokeGet(sourceEntity, selectedProperties.SourceSelectdProperty.KeyProperty).ToString();
				destKeyValue = Dynamic.InvokeGet(destinationEntity, selectedProperties.DestinationSelectedProperty.KeyProperty).ToString();
				hasKeyValue = true;
			}


			var sourceAmountValues = new List<decimal>();
			foreach (var amountProperty in selectedProperties.SourceSelectdProperty.AmountProperties)
			{
				var amountValue = Dynamic.InvokeGet(sourceEntity, amountProperty);
				sourceAmountValues.Add(_amountPropertyProvider.Parse(amountValue));
			}

			var destinationAmountValues = new List<decimal>();
			foreach (var amountProperty in selectedProperties.DestinationSelectedProperty.AmountProperties)
			{
				var amountValue = Dynamic.InvokeGet(destinationEntity, amountProperty);
				destinationAmountValues.Add(_amountPropertyProvider.Parse(amountValue));
			}

			var sourcDateTime = _datePropertyProvider.Parse(sourceDateValue);
			var destDateTime = _datePropertyProvider.Parse(destinationDateValue);
			var dayDiff = Math.Abs((sourcDateTime - destDateTime).TotalDays);
			var amountDiff = Math.Abs(sourceAmountValues.First() - destinationAmountValues.First());


			bool isMatch;
			if (hasKeyValue)
				isMatch = dayDiff <= _toleranceViewModel.DaysTolerance
				          && amountDiff <= _toleranceViewModel.AmountTolerance
				          && _comparerProvider.Compare(sourceKeyValue, destKeyValue, selectedProperties.SelectedComparerType);
			else
				isMatch = dayDiff <= _toleranceViewModel.DaysTolerance && amountDiff <= _toleranceViewModel.AmountTolerance;

			destinationEntity.IsMatch = isMatch;

			if (isMatch)
				_matchedItems.Add(sourceEntity, destinationEntity);

			return isMatch;
		}

		private bool HasKeyValue(SelectedProperties selectedProperties)
		{
			return !string.IsNullOrEmpty(selectedProperties.SourceSelectdProperty.KeyProperty) &&
							!string.IsNullOrEmpty(selectedProperties.DestinationSelectedProperty.KeyProperty);
		}
	}
}
