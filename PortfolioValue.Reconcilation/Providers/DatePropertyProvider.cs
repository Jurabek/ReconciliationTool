﻿using System;
using System.Globalization;
using System.Linq;
using PortfolioValue.Reconcilation.Abstractions.Providers;

namespace PortfolioValue.Reconcilation.Providers
{
	public class DatePropertyProvider : IDatePropertyProvider
	{
		private readonly string[] _formats;

		public DatePropertyProvider()
		{
			var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
			var allpatterns = cultures.SelectMany(c => c.DateTimeFormat.GetAllDateTimePatterns());
			_formats = allpatterns.ToArray();
		}

		public DateTime Parse(dynamic dateTime)
		{
			if (DateTime.TryParseExact(dateTime, _formats, CultureInfo.InvariantCulture, DateTimeStyles.None,
				out DateTime sourcDateTime))
			{
			}
			else if (DateTime.TryParse(dateTime, out sourcDateTime))
			{
			}
			return sourcDateTime;
		}
	}
}
