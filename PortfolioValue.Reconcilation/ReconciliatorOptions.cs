﻿using System.ComponentModel;

namespace PortfolioValue.Reconcilation
{
    public struct ReconciliatorOptions
    {


        /*  add options for 
         * - match description string matching based on some sort of lookup disctionary?
         * - one to many entry matching ? (for futures?)
         * 
         */
        public int DaysTolerance { get; }
        public decimal AmountTolerance { get; }
        public MatchingMethodEnum MatchingMethod { get; }

        public ReconciliatorOptions(int daysTolerance, decimal amountTolerance, MatchingMethodEnum matchingMethod)
        {
            DaysTolerance = daysTolerance;
            AmountTolerance = amountTolerance;
            MatchingMethod = matchingMethod;
        }

        public static ReconciliatorOptions Defaults
            => new ReconciliatorOptions(0, 0, MatchingMethodEnum.OneToOne);
    }

    public enum MatchingMethodEnum
    {
        [Description("One to One")]
        OneToOne,
        [Description("One to Many")]
        OneToMany
    }
}
