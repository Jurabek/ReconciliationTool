﻿using System.Linq;
using PortfolioValue.Reconcilation.Settings.Entities;

namespace PortfolioValue.Reconcilation.Repositories
{
    public interface ISettingsRepository
    {
        IQueryable<FileSettings> Settings { get; }
        void Create(FileSettings fileSettings);
        void Delete(FileSettings fileSettings);
        bool Exists(string filePath);
        FileSettings FindByFilePath(string filePath);
        FileSettings Get(int id);
        void Update(FileSettings fileSettings);
    }
}