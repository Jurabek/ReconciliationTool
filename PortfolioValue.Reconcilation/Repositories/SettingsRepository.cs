﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using PortfolioValue.Reconcilation.Settings;
using PortfolioValue.Reconcilation.Settings.Entities;

namespace PortfolioValue.Reconcilation.Repositories
{
    public class SettingsRepository : ISettingsRepository
    {
        private readonly SettingsDbContext _context;

        public SettingsRepository(SettingsDbContext context)
        {
            _context = context;
        }

        public bool Exists(string filePath)
        {
            return _context.Settings.Any(s => s.FilePath == filePath);
        }

        public void Create(FileSettings fileSettings)
        {
            _context.Settings.Add(fileSettings);
            _context.SaveChanges();
        }

        public void Delete(FileSettings fileSettings)
        {
            _context.Settings.Remove(fileSettings);
            _context.SaveChanges();
        }

        public void Update(FileSettings fileSettings)
        {
            _context.Update(fileSettings);
            _context.SaveChanges();
        }

        public FileSettings Get(int id)
        {
            return _context.Settings.Find(id);
        }

        public FileSettings FindByFilePath(string filePath)
        {
            return _context.Settings.Include(s => s.AmountProperties)
                .FirstOrDefault(s => s.FilePath == filePath);
        }

        public IQueryable<FileSettings> Settings => _context.Settings;

                
    }
}
