﻿using System.Collections.Generic;

namespace PortfolioValue.Reconcilation.Models
{
	public class SelectedProperty
	{
		public IEnumerable<string> AmountProperties { get; set; }

		public string DateProperty { get; set; }

		public string KeyProperty { get; set; }
	}
}
