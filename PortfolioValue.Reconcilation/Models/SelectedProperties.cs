﻿using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.Models
{
	public class SelectedProperties
	{
		public SelectedProperties()
		{
			SourceSelectdProperty = new SelectedProperty();
			DestinationSelectedProperty = new SelectedProperty();
		}

		public SelectedProperty SourceSelectdProperty { get; }

		public SelectedProperty DestinationSelectedProperty { get; }
		
		public int DaysTolerance { get; set; }
		
		public decimal AmountTolerance { get; set; }

		public KeyComparerType SelectedComparerType { get; set; }

		public MatchingMethodType SelectedMatchingMethodType { get; set; }
	}
}
