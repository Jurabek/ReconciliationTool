﻿using System.Collections.Generic;
using System.Linq;

namespace PortfolioValue.Reconcilation
{
    public class Reconciliator
    {
        private readonly List<LedgerEntry> _internalLedger;

        public Reconciliator(List<LedgerEntry> ledger)
        {
            _internalLedger = ledger;

        }

        public void Match(List<LedgerEntry> externalLedger, ReconciliatorOptions options)
        {
            _internalLedger.ForEach(e => e.HasMatchIn(externalLedger.Where(i => !i.IsMatched).ToList(), options));
        }

        public List<LedgerEntry> MatchingEntriesFor(List<LedgerEntry> externalLedger, ReconciliatorOptions options)
                 => _internalLedger
                        .Where(e => e.HasMatchIn(externalLedger, options))
                        .ToList();



        public List<LedgerEntry> MismatchingEntriesFor(List<LedgerEntry> externalLedger, ReconciliatorOptions options)
            => _internalLedger
                .Except(MatchingEntriesFor(externalLedger, options))
                .ToList();


    }
}
