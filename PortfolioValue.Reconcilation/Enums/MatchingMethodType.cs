﻿using System.ComponentModel;

namespace PortfolioValue.Reconcilation.Enums
{
	public enum MatchingMethodType
	{
		[Description("One to One")]
		OneToOne,

		[Description("One to Many")]
		OneToMany
	}
}