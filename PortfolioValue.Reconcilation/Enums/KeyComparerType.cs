﻿using System.ComponentModel;

namespace PortfolioValue.Reconcilation.Enums
{
	public enum KeyComparerType
	{
		[Description("Exact matching")]
		ExactMatch,
		[Description("Left contains to Right")]
		LeftContainsRight,
	}
}