﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Subjects;
using Dynamitey;
using MoreLinq;
using PortfolioValue.Reconcilation.Repositories;
using PortfolioValue.Reconcilation.Settings.Entities;
using ReactiveUI;
using System.Threading.Tasks;
using PortfolioValue.Reconcilation.Abstractions.Loaders;
using PortfolioValue.Reconcilation.Abstractions.Providers;
using PortfolioValue.Reconcilation.Abstractions.ViewModels;
using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.ViewModels
{
	public abstract class BaseItemsViewModel : ReactiveObject, IItemsViewModel
	{
		private readonly ISettingsRepository _settingsRepository;
		private readonly IAmountPropertyProvider _amountPropertyProvider;
		private readonly IExcelLoader _excelLoader;
		private FileSettings _settings;

		public IObservable<IEnumerable<string>> AmountProprtiesChanged { get; }
		public IObservable<string> DatePropertyChanged { get; }
		public IObservable<string> KeyPropertyChanged { get; }
		public ReactiveCommand<Unit, Unit> Load { get; }
		
		protected BaseItemsViewModel(
			IFileLoader fileLoader,
			IExcelLoader excelLoader,
			ISettingsRepository settingsRepository,
			IAmountPropertyProvider amountPropertyProvider)
		{
			_settingsRepository = settingsRepository;
			_amountPropertyProvider = amountPropertyProvider;
			_excelLoader = excelLoader;

			AmountProperties = new ReactiveList<string>();
			AmountProprtiesChanged = new Subject<IEnumerable<string>>();
			DatePropertyChanged = new Subject<string>();
			KeyPropertyChanged = new Subject<string>();

			Load = ReactiveCommand.Create(() =>
			{
				fileLoader.Open(async (filePath, fileType) =>
				{
					await LoadData(filePath, fileType);
				});
			});
		}

		private async Task LoadData(string filePath, FileType fileType)
		{
			try
			{
				IsLoading = true;
				await LoadItems(filePath, fileType).ContinueWith(t =>
				{
					if (t.IsFaulted && t.Exception != null)
						throw t.Exception;

					LoadSettings();
				});

			}
			catch (Exception ex)
			{
				MessageBus.Current.SendMessage(ex.InnerException ?? ex);
			}
			finally
			{
				IsLoading = false;
			}
		}

		private ObservableCollection<string> _properties;
		public ObservableCollection<string> Properties
		{
			get => _properties;
			private set => this.RaiseAndSetIfChanged(ref _properties, value);
		}

		public ObservableCollection<dynamic> OriginalItems { get; private set; }

		private ObservableCollection<dynamic> _items;
		public ObservableCollection<dynamic> Items
		{
			get => _items;
			set => this.RaiseAndSetIfChanged(ref _items, value);
		}

		private ObservableCollection<dynamic> _matchedItemsForSelectedItem;
		public ObservableCollection<dynamic> MatchedItemsForSelectedItem
		{
			get => _matchedItemsForSelectedItem;
			set => this.RaiseAndSetIfChanged(ref _matchedItemsForSelectedItem, value);
		}

		private ReactiveList<string> _amountProperties;
		public ReactiveList<string> AmountProperties
		{
			get => _amountProperties;
			private set
			{
				_amountProperties = value;
				if (value != null)
					foreach (var propertyName in value)
						AddAmoutPropertyToSettings(propertyName);

				this.RaisePropertyChanged();
			}
		}


		private bool _isLoading;
		public bool IsLoading
		{
			get => _isLoading;
			set => this.RaiseAndSetIfChanged(ref _isLoading, value);
		}

		private dynamic _selectedItem;
		public virtual dynamic SelectedItem
		{
			get => _selectedItem;
			set { _selectedItem = value; this.RaisePropertyChanged(); }
		}

		private string _dateProperty;
		public string DateProperty
		{
			get => _dateProperty;
			set
			{
				if (_dateProperty == value)
					return;

				_dateProperty = value;
				if (_settings.DateProperty != value)
				{
					_settings.DateProperty = value;
					_settingsRepository.Update(_settings);
					RemoveAmountIfExist(value);
				}
				this.RaisePropertyChanged();
			}
		}

		private void RemoveAmountIfExist(string value)
		{
			if (_settings.AmountProperties.Select(a => a.Name).Contains(value))
			{
				var amountProperty = _settings
					.AmountProperties.FirstOrDefault(a => a.Name == value);

				_settings.AmountProperties.Remove(amountProperty);
				_settingsRepository.Update(_settings);
			}
		}

		private string _keyProperty;
		public string KeyProperty

		{
			get => _keyProperty;
			set
			{
				if(_keyProperty == value)
					return;

				_keyProperty = value;
				if (_settings.KeyProperty != value)
				{
					_settings.KeyProperty = value;
					_settingsRepository.Update(_settings);
					RemoveAmountIfExist(value);
				}
			} 
		}


		private string _path;
		public string Path
		{
			get => _path;
			set => this.RaiseAndSetIfChanged(ref _path, value);
		}

		public void AddAmountProperty(string propertyName)
		{
			if (AmountProperties.Contains(propertyName))
				return;

			AmountProperties.Add(propertyName);
			AddAmoutPropertyToSettings(propertyName);
			ValidateAmountProperty(propertyName);
		}

		private void ValidateAmountProperty(string propertyName)
		{
			foreach (var item in Items)
				_amountPropertyProvider.Validate(item, propertyName);
		}

		public void RemoveAmountProperty(string propertyName)
		{
			if (!AmountProperties.Contains(propertyName))
				return;

			AmountProperties.Remove(propertyName);
			RemoveAmountPropertyFromSettings(propertyName);
		}

		public void Reset()
		{
			Items = new ObservableCollection<dynamic>(OriginalItems.Copy());
			AmountProperties.ForEach(ValidateAmountProperty);
		}

		private async Task LoadItems(string filePath, FileType fileType)
		{
			Path = filePath;
			var data = await _excelLoader.GetData(filePath, fileType);
			data.ForEach(i => i.IsMatch = null);

			var properties = Dynamic.GetMemberNames(data.FirstOrDefault());
			Properties = new ObservableCollection<string>(properties);

			OriginalItems = new ObservableCollection<dynamic>(data.Copy());
			Items = data;
		}

		private void LoadSettings()
		{
			if (_settingsRepository.Exists(Path))
			{
				_settings = _settingsRepository.FindByFilePath(Path);
				DateProperty = _settings.DateProperty;
				KeyProperty = _settings.KeyProperty;

				var amountProperties = _settings.AmountProperties.Select(ap => ap.Name);
				AmountProperties = new ReactiveList<string>(amountProperties);
				AmountProperties.ForEach(ValidateAmountProperty);
				((Subject<string>)DatePropertyChanged).OnNext(DateProperty);
				((Subject<string>)KeyPropertyChanged).OnNext(KeyProperty);
				((Subject<IEnumerable<string>>)AmountProprtiesChanged).OnNext(AmountProperties);
			}
			else
			{
				_settings = new FileSettings { FilePath = Path };
				_settingsRepository.Create(_settings);
			}
		}

		private void AddAmoutPropertyToSettings(string propertyName)
		{
			if (_settings.AmountProperties.Any(p => p.Name == propertyName))
				return;

			var property = new AmountProperty { Name = propertyName, FileSettingsId = _settings.Id };
			_settings.AmountProperties.Add(property);
			_settingsRepository.Update(_settings);
		}

		private void RemoveAmountPropertyFromSettings(string propertyName)
		{
			if (!_settings.AmountProperties.Any(p => p.Name == propertyName))
				return;

			var property = _settings.AmountProperties.FirstOrDefault(a => a.Name == propertyName);
			_settings.AmountProperties.Remove(property);
			_settingsRepository.Update(_settings);
		}
	}
}
