using ReactiveUI;

namespace PortfolioValue.Reconcilation.ViewModels
{
    public class ToleranceViewModel : ReactiveObject
    {
        private int _daysTolerance;
        public int DaysTolerance
        {
            get => _daysTolerance;
            set => this.RaiseAndSetIfChanged(ref _daysTolerance, value);
        }

        private decimal _amountTolerance;

        public decimal AmountTolerance
        {
            get => _amountTolerance;
            set => this.RaiseAndSetIfChanged(ref _amountTolerance, value);
        }
    }
}