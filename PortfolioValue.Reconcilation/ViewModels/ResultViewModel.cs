﻿using ReactiveUI;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace PortfolioValue.Reconcilation.ViewModels
{
    public class ResultViewModel : ReactiveObject
    {
        private readonly DestinationItemsViewModel _destinationItemsViewModel;
        private readonly SourceItemsViewModel _sourceItemsViewModel;

        public ResultViewModel(DestinationItemsViewModel destinationItemsViewModel, SourceItemsViewModel sourceItemsViewModel)
        {
            _destinationItemsViewModel = destinationItemsViewModel;
            _sourceItemsViewModel = sourceItemsViewModel;

            LoadMatchedItems = ReactiveCommand.Create(() =>
            {
                if (_sourceItemsViewModel.Items.Any(d => d.IsMatch == true))
                    SourceItems = _sourceItemsViewModel.Items
                                        .Where(d => d.IsMatch == true)
                                        .Select(i => DynamicHelper.Copy(i));
                else
                    SourceItems = Enumerable.Empty<dynamic>();

                DestinationItems = _destinationItemsViewModel.Items
                                        .Where(d => d.IsMatch == true)
                                        .Select(i => DynamicHelper.Copy(i));
            });

            LoadNotMatchedItems = ReactiveCommand.Create(() =>
            {
                if (_sourceItemsViewModel.Items.Any(d => d.IsMatch == false))
                    SourceItems = _sourceItemsViewModel.Items
                                        .Where(d => d.IsMatch == false)
                                        .Select(i => DynamicHelper.Copy(i));
                else
                    SourceItems = Enumerable.Empty<dynamic>();

                DestinationItems = _destinationItemsViewModel.Items
                                        .Where(d => d.IsMatch == false)
                                        .Select(i => DynamicHelper.Copy(i));
            });
        }

        private IEnumerable<dynamic> _sourceItems;
        public IEnumerable<dynamic> SourceItems
        {
            get { return _sourceItems; }
            private set { this.RaiseAndSetIfChanged(ref _sourceItems, value); }
        }

        private IEnumerable<dynamic> _destinationItems;
        public IEnumerable<dynamic> DestinationItems
        {
            get { return _destinationItems; }
            set { this.RaiseAndSetIfChanged(ref _destinationItems, value); }
        }

        public ICommand LoadMatchedItems { get; private set; }

        public ICommand LoadNotMatchedItems { get; private set; }
    }
}
