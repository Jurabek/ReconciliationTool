using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive;
using ReactiveUI;
using System.Reactive.Linq;
using System.Threading;
using PortfolioValue.Reconcilation.Abstractions.Adapters;
using PortfolioValue.Reconcilation.Abstractions.Providers;
using PortfolioValue.Reconcilation.Abstractions.ViewModels;
using PortfolioValue.Reconcilation.Enums;

namespace PortfolioValue.Reconcilation.ViewModels
{

	public class ReconcilationViewModel : ReactiveObject, IReconcilationViewModel
	{
		private CancellationTokenSource _reconcilationCancellationTokenSource;
		
		public DestinationItemsViewModel DestinationItemsViewModel { get; set; }

		public SourceItemsViewModel SourceItemsViewModel { get; private set; }
		
		public ReactiveCommand<Unit, Unit> StartReconciliation { get; set; }

		public ReactiveCommand<Unit, Unit> ResetCommand { get; set; }

		public ReactiveCommand StopCommand { get; set; }

		public ToleranceViewModel ToleranceViewModel { get; set; }

		private MatchingMethodType _matchingMethodType;
		public MatchingMethodType MatchingMethodType
		{
			get => _matchingMethodType;
			set => this.RaiseAndSetIfChanged(ref _matchingMethodType, value);
		}

		private KeyComparerType _keyComapererType;
		public KeyComparerType KeyComapererType
		{
			get => _keyComapererType;
			set => this.RaiseAndSetIfChanged(ref _keyComapererType, value);
		}

		private bool _hasAmountProperty;
		public bool HasAmountProperty
		{
			get => _hasAmountProperty;
			set => this.RaiseAndSetIfChanged(ref _hasAmountProperty, value);
		}


		private bool _isReconciliating;
		public bool IsReconciliating
		{
			get => _isReconciliating;
			set => this.RaiseAndSetIfChanged(ref _isReconciliating, value);
		}

		private bool _isReconcilated;
		public bool IsReconcilated
		{
			get => _isReconcilated;
			set => this.RaiseAndSetIfChanged(ref _isReconcilated, value);
		}

		public ReconcilationViewModel(
			DestinationItemsViewModel destinationItemsViewModel,
			SourceItemsViewModel sourceItemsViewModel,
			ToleranceViewModel toleranceViewModel,
			ISelectedPropertiesAdapter selectedPropertiesAdapter,
			IReconciliationProvider reconciliationProvider)
		{
			DestinationItemsViewModel = destinationItemsViewModel;
			SourceItemsViewModel = sourceItemsViewModel;
			ToleranceViewModel = toleranceViewModel;


			this.WhenAnyObservable(x => x.SourceItemsViewModel.AmountProperties.Changed, x => x.DestinationItemsViewModel.AmountProperties.Changed)
				.Subscribe(changed =>
				{
					if (changed.Action == NotifyCollectionChangedAction.Add && SourceItemsViewModel.AmountProperties.Any() &&
						DestinationItemsViewModel.AmountProperties.Any())
						HasAmountProperty = true;
					else if (changed.Action == NotifyCollectionChangedAction.Remove &&
							 (!SourceItemsViewModel.AmountProperties.Any() || !DestinationItemsViewModel.AmountProperties.Any()))
						HasAmountProperty = false;
				});

			this.WhenAnyObservable(d => d.DestinationItemsViewModel.AmountProprtiesChanged, s => s.SourceItemsViewModel.AmountProprtiesChanged)
				.ObserveOn(RxApp.MainThreadScheduler)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
				{
					var da = DestinationItemsViewModel.AmountProperties;
					var sa = SourceItemsViewModel.AmountProperties;

					if (da != null && da.Any() && sa != null && sa.Any())
						HasAmountProperty = true;
				});

			var canStart = this.WhenAny(
				vm => vm.HasAmountProperty,
				sd => sd.SourceItemsViewModel.DateProperty,
				dd => dd.DestinationItemsViewModel.DateProperty,
				(ha, sd, dd) => ha.Value
							&& !string.IsNullOrEmpty(sd.Value)
							&& !string.IsNullOrEmpty(dd.Value));


			StartReconciliation = ReactiveCommand.CreateFromTask(async () =>
			{
				if (IsReconcilated)
					ResetCommand.Execute().Subscribe();

				IsReconciliating = true;

				DestinationMatchedItems = new Dictionary<dynamic, dynamic>();
				SourceMatchedItems = new Dictionary<dynamic, dynamic>();
				_reconcilationCancellationTokenSource = new CancellationTokenSource();

				var selectedProperties = selectedPropertiesAdapter.GetSelectedProperties(SourceItemsViewModel, DestinationItemsViewModel, ToleranceViewModel, this);
				var matchedItems = await reconciliationProvider.Reconcilate(SourceItemsViewModel.Items,
					DestinationItemsViewModel.Items, 
					selectedProperties, 
					ToleranceViewModel, 
					_reconcilationCancellationTokenSource.Token);

				foreach (var matchedItem in matchedItems)
				{
					SourceMatchedItems.Add(matchedItem.Key, matchedItem.Value);
					DestinationMatchedItems.Add(matchedItem.Value, matchedItem.Key);
				}

				IsReconciliating = false;
				IsReconcilated = true;

			}, canStart);

			StopCommand = ReactiveCommand.Create(() =>
			{
				_reconcilationCancellationTokenSource.Cancel();
				IsReconciliating = false;

			}, this.WhenAnyValue(x => x.IsReconciliating));
			
			ResetCommand = ReactiveCommand.Create(() =>
			{

				SourceItemsViewModel.Reset();
				DestinationItemsViewModel.Reset();

				SourceMatchedItems.Clear();
				DestinationMatchedItems.Clear();

				IsReconcilated = false;

			}, this.WhenAnyValue(x => x.IsReconcilated));
		}

		private IDictionary<dynamic, dynamic> _sourceMatchedItems;
		public IDictionary<dynamic, dynamic> SourceMatchedItems
		{
			get => _sourceMatchedItems;
			set => this.RaiseAndSetIfChanged(ref _sourceMatchedItems, value);
		}

		private IDictionary<dynamic, dynamic> _destinationMatchedItems;
		public IDictionary<dynamic, dynamic> DestinationMatchedItems
		{
			get => _destinationMatchedItems;
			set => this.RaiseAndSetIfChanged(ref _destinationMatchedItems, value);
		}
	}
}