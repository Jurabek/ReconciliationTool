﻿using PortfolioValue.Reconcilation.Abstractions.Loaders;
using PortfolioValue.Reconcilation.Abstractions.Providers;
using PortfolioValue.Reconcilation.Repositories;

namespace PortfolioValue.Reconcilation.ViewModels
{
	public class DestinationItemsViewModel : BaseItemsViewModel
	{
		public DestinationItemsViewModel(
			IFileLoader fileLoader,
			IExcelLoader excelLoader,
			ISettingsRepository repository,
			IAmountPropertyProvider amountPropertyProvider) 
			: base(fileLoader, excelLoader, repository, amountPropertyProvider)
		{
		}
	}
}
