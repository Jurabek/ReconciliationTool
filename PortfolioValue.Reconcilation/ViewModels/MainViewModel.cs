﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using PortfolioValue.Reconcilation.Abstractions.ViewModels;
using ReactiveUI;

namespace PortfolioValue.Reconcilation.ViewModels
{
    public class MainViewModel : ReactiveObject, IDisposable
    {
        public ReconcilationViewModel ReconcilationViewModel { get; }
        public SourceItemsViewModel SourceItemsViewModel { get; }
        public DestinationItemsViewModel DestinationItemsViewModel { get; }

        public ResultViewModel ResultViewModel { get; set; }

        private readonly ObservableAsPropertyHelper<bool> _isLoaded;
        public bool IsLoaded => _isLoaded.Value;

	    private readonly IDisposable _selectedSubscribtion;
		
        public MainViewModel(
              IReconcilationViewModel reconcilationViewModel
            , SourceItemsViewModel sourceItemsViewModel
            , DestinationItemsViewModel destinationItemsViewModel
            , ResultViewModel resultViewModel)
        {
            ReconcilationViewModel = reconcilationViewModel as ReconcilationViewModel;
            SourceItemsViewModel = sourceItemsViewModel;
            DestinationItemsViewModel = destinationItemsViewModel;
            ResultViewModel = resultViewModel;

            _selectedSubscribtion = this.WhenAnyValue(x => x.SourceItemsViewModel.SelectedItem)
                .Where(i => i != null)
                .Subscribe(d =>
                {
                    if (ReconcilationViewModel.SourceMatchedItems != null && ReconcilationViewModel.SourceMatchedItems.Any())
                    {
                        DestinationItemsViewModel.SelectedItem = null;
                        if (ReconcilationViewModel.SourceMatchedItems.ContainsKey(d))
                        {
                            var data = ReconcilationViewModel.SourceMatchedItems[d];
                            var source = new ObservableCollection<dynamic> { data };
                            DestinationItemsViewModel.MatchedItemsForSelectedItem = source;
                        }
                    }
                });


            this.WhenAnyValue(x => x.DestinationItemsViewModel.SelectedItem)
                .Where(i => i != null)
                .Subscribe(d =>
                {
                    if (ReconcilationViewModel.DestinationMatchedItems != null && ReconcilationViewModel.DestinationMatchedItems.Any())
                    {
	                    SourceItemsViewModel.SelectedItem = null;
						if (ReconcilationViewModel.DestinationMatchedItems.ContainsKey(d))
	                    {
		                    var destination = new ObservableCollection<dynamic>
		                    {
			                    ReconcilationViewModel.DestinationMatchedItems[d]
		                    };
		                    SourceItemsViewModel.MatchedItemsForSelectedItem = destination;
						}
                    }
                });

            var hasItems = this.WhenAnyValue(x => x.DestinationItemsViewModel.Items, x => x.SourceItemsViewModel.Items)
                .Where(i => i.Item1 != null && i.Item1.Any() && i.Item2 != null && i.Item2.Any())
                .Any();
			
            _isLoaded = hasItems.ToProperty(this, x => x.IsLoaded);
			
            //this.WhenAnyValue(x => x.ResultViewModel.SourceItems)
            //    .Where(items => items != null)
            //    .Subscribe(items =>
            //    {
            //        if (items.Any())
            //            SourceItemsViewModel.Items = new ObservableCollection<dynamic>(items);
            //        else
            //            SourceItemsViewModel.Items.Clear();
            //    });

            //this.WhenAnyValue(x => x.ResultViewModel.DestinationItems)
            //    .Where(items => items != null)
            //    .Subscribe(items =>
            //    {
            //        if (items.Any())
            //        {
            //            DestinationItemsViewModel.Items = new ObservableCollection<dynamic>(items);
            //        }
            //        else
            //        {
            //            DestinationItemsViewModel.Items.Clear();
            //        }
            //    });
        }

        public void Dispose()
        {
            _selectedSubscribtion.Dispose();
        }
    }
}
