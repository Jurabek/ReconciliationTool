﻿using System;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PortfolioValue.Reconcilation.Abstractions.Loaders;
using PortfolioValue.Reconcilation.Abstractions.Providers;
using PortfolioValue.Reconcilation.Enums;
using PortfolioValue.Reconcilation.Repositories;
using PortfolioValue.Reconcilation.Settings.Entities;
using PortfolioValue.Reconcilation.ViewModels;
using ReactiveUI;

namespace PortfolioValue.Reconciliation.Tests.ViewModels
{
    [TestClass]
    public class ItemsViewModelTest
    {
        private Mock<IFileLoader> _fileLoader;
        private Mock<IExcelLoader> _excelLoader;
        private Mock<ISettingsRepository> _settingsRepository;
        private BaseItemsViewModel _viewModel;
        string filePath = "test_path";

        [TestInitialize]
        public void Init()
        {
            _fileLoader = new Mock<IFileLoader>();
            _excelLoader = new Mock<IExcelLoader>();
            _settingsRepository = new Mock<ISettingsRepository>();
			var amoutPropertyProvider = new Mock<IAmountPropertyProvider>();
            _viewModel = new SourceItemsViewModel(
                _fileLoader.Object,
                _excelLoader.Object,
                _settingsRepository.Object, amoutPropertyProvider.Object);

            _fileLoader.Setup(x => x.Open(It.IsAny<Action<string, FileType>>()))
                .Callback<Action<string, FileType>>(action =>
                {
                    action.Invoke(filePath, FileType.Excel);
                });
        }

        [TestMethod]
        public void LoadCommand_should_load_items_from_excel()
        {
            var dynamicObject1 = new ExpandoObject();
            var dynamicObject2 = new ExpandoObject();
            var dynamicObject3 = new ExpandoObject();

            _excelLoader.Setup(x => x.GetData(It.IsAny<string>(), It.IsAny<FileType>()))
                .Returns(Task.FromResult(new ObservableCollection<dynamic>()
                {
                    dynamicObject1,
                    dynamicObject2,
                    dynamicObject3
                }));

            _settingsRepository.Setup(x => x.Exists(It.IsAny<string>()))
                .Returns(true);

            _settingsRepository.Setup(x => x.FindByFilePath(filePath))
                .Returns(new FileSettings());

            _viewModel.Load.Execute().Subscribe();

            Assert.AreSame(dynamicObject1, _viewModel.Items.FirstOrDefault());
        }

        [TestMethod]
        public void LoadCommand_should_throws_exception_with_wrong_excel_file()
        {
            var error = "could_not_load_file";
            _excelLoader.Reset();

            _excelLoader.Setup(x => x.GetData(It.IsAny<string>(), It.IsAny<FileType>()))
                .Throws(new Exception(error));

            MessageBus.Current.Listen<Exception>()
                .Subscribe(ex =>
                {
                    Assert.AreEqual(error, ex.InnerException?.Message);
                });

            _viewModel.Load.Execute().Subscribe();
        }
        
    }
}
