//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.Collections.Generic;
//using PortfolioValue.Reconcilation;

//namespace PortfolioValue.Reconciliation.Tests
//{
//    [TestClass]
//    public class ReconciliatorOneToOneTests
//    {
//        List<LedgerEntry> _fakeCustodianData;
//        List<LedgerEntry> _fakeInternalData;

//        [TestInitialize]

//        public void Setup()
//        {/*
//            3 entries match perfectly
//            1 entry matches with day tolerance 1 day
//            1 entry matches with amount tolerance of 500

//             */
//            _fakeCustodianData = new List<LedgerEntry>
//            {
//                new LedgerEntry{Date= new DateTime(2016,1,1), Description = "Entry #1", Amount = 1000M },
//                new LedgerEntry{Date= new DateTime(2016,1,2), Description = "Entry #2", Amount = 2000M },
//                new LedgerEntry{Date= new DateTime(2016,1,3), Description = "Entry #3", Amount = 3000M },
//                new LedgerEntry{Date= new DateTime(2016,1,4), Description = "Entry #4", Amount = 4000M },
//                new LedgerEntry{Date= new DateTime(2016,1,5), Description = "Entry #5", Amount = 5000M },

//            };

//            _fakeInternalData = new List<LedgerEntry>
//            {
//                new LedgerEntry{Date= new DateTime(2016,1,1), Description = "Entry #1", Amount = 1000M },
//                new LedgerEntry{Date= new DateTime(2016,1,2), Description = "Entry #2", Amount = 2000M },
//                new LedgerEntry{Date= new DateTime(2016,1,3), Description = "Entry #3", Amount = 3000M },
//                new LedgerEntry{Date= new DateTime(2016,1,4), Description = "Entry #4", Amount = 4500M },
//                new LedgerEntry{Date= new DateTime(2016,1,6), Description = "Entry #6", Amount = 5000M },       // mismatched entry

//            };
//        }
//        [TestMethod]
//        public void MatchingEntriesTests()

//        {
//            var options = new ReconciliatorOptions(daysTolerance: 0, amountTolerance: 0M, 
//                                                matchingMethod:MatchingMethodType.OneToOne);

//            var sut = new Reconciliator(_fakeInternalData);
//            var matched = sut.MatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(matched.Count, 3);


//        }
//        [TestMethod]
//        public void MismatchingEntriesTests()

//        {
//            var options = new ReconciliatorOptions(daysTolerance: 0, amountTolerance: 0M,
//                                                matchingMethod: MatchingMethodType.OneToOne);
//            var sut = new Reconciliator(_fakeInternalData);
//            var matched = sut.MismatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(matched.Count, 2);

//        }

//        [TestMethod]
//        public void MatchingEntries_WithDayTolerance_Tests()

//        {
//            var options = new ReconciliatorOptions(daysTolerance: 1, amountTolerance: 0M,
//                                                matchingMethod: MatchingMethodType.OneToOne);

//            var sut = new Reconciliator(_fakeInternalData);

//            var matched = sut.MatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(matched.Count, 4);

//            var mismatched = sut.MismatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(mismatched.Count, 1);

//        }


//        [TestMethod]
//        public void MatchingEntries_WithAmountTolerance_Tests()

//        {
//            var options = new ReconciliatorOptions(daysTolerance: 0, amountTolerance: 500M,
//                                                matchingMethod: MatchingMethodType.OneToOne);

//            var sut = new Reconciliator(_fakeInternalData);
//            var matched = sut.MatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(matched.Count, 4);

//            var mismatched = sut.MismatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(mismatched.Count, 1);

//        }

//        [TestMethod]
//        public void MatchingEntries_WithBothAmountAndDateTolerance_Tests()

//        {
//            var options = new ReconciliatorOptions(daysTolerance: 1, amountTolerance: 500M,
//                                                matchingMethod: MatchingMethodType.OneToOne);
//            var sut = new Reconciliator(_fakeInternalData);
//            var matched = sut.MatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(matched.Count, 5);

//            var mismatched = sut.MismatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(mismatched.Count, 0);

//        }
//    }
//}
