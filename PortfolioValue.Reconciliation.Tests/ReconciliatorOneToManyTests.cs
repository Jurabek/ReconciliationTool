//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.Collections.Generic;
//using PortfolioValue.Reconcilation;

//namespace PortfolioValue.Reconciliation.Tests
//{
//    [TestClass]
//    public class ReconciliatorOneToManyTests
//    { 
        
//        [TestMethod, Ignore]
//        public void MatchingContinouslyAdjacentEntriesTests()

//        {
//            // Internal Ledger Entry matches first (3) continuously adjacent entries in external (with no other entries interspersed within the sequence)
//            var _fakeCustodianData = new List<LedgerEntry>
//            {
//                new LedgerEntry{Date= new DateTime(2016,1,1), Description = "10 S&P DEC", Amount = 1000M },
//                new LedgerEntry{Date= new DateTime(2016,1,2), Description = "10 S&P DEC", Amount = 2000M },
//                new LedgerEntry{Date= new DateTime(2016,1,3), Description = "10 S&P DEC", Amount = 3000M },
//                new LedgerEntry{Date= new DateTime(2016,1,4), Description = "Entry xxx yyy", Amount = 4000M },


//            };

//            var _fakeInternalData = new List<LedgerEntry>
//            {
//                new LedgerEntry{Date= new DateTime(2016,1,1), Description = "Entry #1", Amount = 6000M }, 
//                new LedgerEntry{Date= new DateTime(2016,1,2), Description = "Entry #2", Amount = 2000M },


//            };
//            var options = new ReconciliatorOptions(daysTolerance: 0, amountTolerance: 0M, 
//                    matchingMethod:MatchingMethodType.OneToMany);

//            var sut = new Reconciliator(_fakeInternalData);

//            var matched = sut.MatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(matched.Count,1);

//            var unmatched = sut.MatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(unmatched.Count, 1);
//        }



//        [TestMethod, Ignore]
//        public void MatchingSuccessivelyAdjacentEntriesTests()

//        {
//            // Internal Ledger Entry matches first (3) successively adjacent entries in external (with other entries interspersed within the sequence)
//            var _fakeCustodianData = new List<LedgerEntry>
//            {
//                new LedgerEntry{Date= new DateTime(2016,1,1), Description = "10 S&P DEC", Amount = 1000M },
//                new LedgerEntry{Date= new DateTime(2016,1,1), Description = "Intersperces Entry Here....", Amount = 500M },
//                new LedgerEntry{Date= new DateTime(2016,1,2), Description = "10 S&P DEC", Amount = 2000M },
//                new LedgerEntry{Date= new DateTime(2016,1,3), Description = "10 S&P DEC", Amount = 3000M },
//                new LedgerEntry{Date= new DateTime(2016,1,4), Description = "Entry xxx yyy", Amount = 4000M },


//            };

//            var _fakeInternalData = new List<LedgerEntry>
//            {
//                new LedgerEntry{Date= new DateTime(2016,1,1), Description = "Entry #1", Amount = 6000M },
//                new LedgerEntry{Date= new DateTime(2016,1,2), Description = "Entry #2", Amount = 2000M },
//                new LedgerEntry{Date= new DateTime(2016,1,3), Description = "Entry #2", Amount = 600M },

//            };
//            var options = new ReconciliatorOptions(daysTolerance: 0, amountTolerance: 0M,
//                    matchingMethod: MatchingMethodType.OneToMany);

//            var sut = new Reconciliator(_fakeInternalData);

//            var matched = sut.MatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(matched.Count, 1);

//            var unmatched = sut.MatchingEntriesFor(_fakeCustodianData, options);
//            Assert.AreEqual(unmatched.Count, 2);
//        }


//    }
//}
